<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
      
    <xsl:output method="text" encoding="UTF-8" indent="no" />
    
    <xsl:param name="delim" select="';'"/>
    <xsl:param name="quote" select="'&quot;'"/>
    <xsl:variable name="newline" select="'&#10;'"/>
    
    <xsl:template match="/">
        <xsl:value-of select="$quote"/>
        <xsl:text>URI</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>rdf:type</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
                
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:notation</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>broaderId</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:prefLabel@es</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:altLabel@es</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:definition@es</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:scopeNote@es</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:related</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:exactMatch</xsl:text>
        <xsl:value-of select="$quote"/>

        <xsl:value-of select="$newline"/>
        
        <xsl:apply-templates/>

    </xsl:template>
    
    <xsl:template match="root">
        <!--  listPerson URI       -->
        <xsl:value-of select="$quote"/>
        <xsl:text>1</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  rdf:type -->
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:Collection</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:notation (Original xml:id) -->
        <xsl:value-of select="$quote"/>
        <xsl:text>not-att_tesauro_de_personas</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:broader Empty ... set it manually ? -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:prefLabel -->
        <xsl:value-of select="$quote"/>
        <xsl:text>not-att_tesauro_de_personas</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:definition -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:scopeNote -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:related -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:exactMatch -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$newline"/>
        
        <xsl:apply-templates select="listPerson"/>   
    </xsl:template>
    
    <xsl:template match="listPerson">
        <!--  listPerson URI       -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@ana"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  rdf:type -->
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:Concept</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:notation (Original xml:id) -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="head"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:broader Empty ... set it manually ? -->
        <xsl:value-of select="$quote"/>
        <xsl:text></xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:prefLabel -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="head"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:definition -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:scopeNote -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:related -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:exactMatch -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$newline"/>
        
        <xsl:apply-templates select="person[@xml:id]"/>
        
    </xsl:template>
    
    <xsl:template match="person">
        
        <!--  person URI       -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@ana"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  rdf:type -->
        <xsl:value-of select="$quote"/>
        <xsl:text>skos:Concept</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:notation (Original xml:id) -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!-- skos:broader -->
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="ancestor::listPerson/@ana"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
               
        <!-- COL prefLabel : slmt le contenu de forname + surname    -->
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="persName/forename/text()[normalize-space()]"/>
        <xsl:text> </xsl:text>
        <xsl:value-of select="persName/surname/text()"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!-- COL altLabel : décompose le contenu en appellant des templates : forename / surname, forename redécomposé ensuite   -->
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="persName/forename"/>
        <xsl:apply-templates select="persName/surname"/>
        <xsl:apply-templates select="persName/addName"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:definition -->
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="persName/roleName"/>
        <xsl:apply-templates select="birth"/>
        <xsl:apply-templates select="death"/>
        <xsl:apply-templates select="sex"/>
        <xsl:apply-templates select="nationality"/>
        <xsl:apply-templates select="faith"/>
        <xsl:apply-templates select="education"/>
        <xsl:apply-templates select="occupation"/>
        <xsl:apply-templates select="affiliation"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:scopeNote -->
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="note"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:related -->
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="note/persName"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <!--  skos:exactMatch -->
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="ptr"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$newline"/>
        <!--<xsl:apply-templates/>-->
    </xsl:template>
   
    <xsl:template match="forename">
        <xsl:if test="text() != ''"> 
        <!--<xsl:apply-templates /> utiliser un apply templates ramènerait tout le texte enfant de forename (genName aussi par exemple)-->
            <xsl:text>forename: </xsl:text>
            <xsl:value-of select="text()[normalize-space()]"/>

            <xsl:if test="//*/text() !=''">
             <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
<!--        Je pourrais mettre un text et un foreach remove add or remove spaces pour aérer ... je ne sais pas ?-->
        <xsl:apply-templates select="genName"/>
        <xsl:apply-templates select="surname" />
    </xsl:template>
    
    <xsl:template match="genName">
        <xsl:if test="text() != ''">
            <xsl:choose>
                <xsl:when test="@type">
                    <xsl:if test="@type='epiteto'">
                        <xsl:text>epiteto: </xsl:text>
                        <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
                        <xsl:if test="following::*/text() != ''">
                            <xsl:text> ## </xsl:text>
                        </xsl:if>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>genName: </xsl:text>
                    <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
                    <xsl:if test="following-sibling::*/normalize-space() != ''">
                        <xsl:text> ## </xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>    
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="surname">
        <xsl:if test="text() != ''"> <!--  if not null -->
            <xsl:text>surname: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            <xsl:if test="following-sibling::addName/text() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
       <xsl:template match="addName">
        <xsl:if test="text() != ''"> <!--  if not null -->
            <xsl:text>addName: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>

            <xsl:if test="following-sibling::addName/text() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
  
    <!-- FIN COL AltLabel -->
    

    
    
    <xsl:template match="birth">
<!--        ne conserver que la date en @-->
        <xsl:if test="@when != '0001-01-01'" >
            <xsl:if test="@when != '0001'">
            <xsl:text>when-birth: </xsl:text>
            <xsl:value-of select="@when"/>
             <xsl:text> ##</xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="./placeName != ''" >
            <xsl:text>birth-place: </xsl:text>
            <xsl:value-of select="normalize-space(translate(./placeName,'&#xA;',''))"/>
            <xsl:if test="./placeName/@type" >
                <xsl:text>|type: </xsl:text>
                <xsl:value-of select="./placeName/@type"/>
            </xsl:if>
            <xsl:text> ##</xsl:text>
        </xsl:if>
        <xsl:if test="./country != ''" >
            <xsl:text>birth-country: </xsl:text>
            <xsl:value-of select="normalize-space(translate(./country,'&#xA;',''))"/>
            <xsl:text> ##</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="death">
        <!--  <xsl:if test="text() != ''">
            <xsl:value-of select="."/>
            <xsl:text> @birth</xsl:text>                            
        </xsl:if>-->
        <!--        ne conserver que la date en @-->
        <xsl:if test="@when != '0001-01-01'" >
            <xsl:if test="@when != '0001'">
                <xsl:text>when-death:</xsl:text>
                <xsl:value-of select="@when"/>
                <xsl:text> ##</xsl:text>
            </xsl:if>
        </xsl:if>
        <xsl:if test="./placeName != ''" >
            <xsl:text>death-place: </xsl:text>
            <xsl:value-of select="normalize-space(translate(./placeName,'&#xA;',''))"/>
            <xsl:if test="./placeName/@type" >
                <xsl:text>|type: </xsl:text>
                <xsl:value-of select="./placeName/@type"/>
            </xsl:if>
            <xsl:text> ##</xsl:text>
        </xsl:if>
        <xsl:if test="./country != ''" >
            <xsl:text>death-country: </xsl:text>
            <xsl:value-of select="normalize-space(translate(./country,'&#xA;',''))"/>
            <xsl:text> ##</xsl:text>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="sex">
        <xsl:if test="text() != ''" >
        <xsl:text>sex: </xsl:text>
        <xsl:value-of select="."/>
            <xsl:if test="following-sibling::*[not(self::note)]/normalize-space() != ''">
            <xsl:text> ## </xsl:text>
        </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="nationality">
        <xsl:if test="text() != ''" >
            <xsl:text>nationality: </xsl:text>
            <xsl:value-of select="."/>    
            <xsl:if test="following-sibling::*[not(self::note)]/normalize-space() != ''">
            <xsl:text> ## </xsl:text>
        </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="faith">
        <xsl:if test="text() != ''" >
            <xsl:text>faith: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            
            
            <xsl:if test="following-sibling::*[not(self::note)]/normalize-space() != ''">
            <xsl:text> ## </xsl:text>
        </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="education">
        <xsl:if test="text() != ''" >
            <xsl:text>education: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            <xsl:if test="following-sibling::*[not(self::note)]/normalize-space() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="occupation">
        <xsl:if test="text() != ''" >
            <xsl:text>occupation: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            
            <xsl:if test="date/@from != ''">
                <xsl:text>|from:</xsl:text>
                <xsl:value-of select="date/@from"/>
            </xsl:if>         
            <xsl:if test="date/@to != ''">
                <xsl:text>|to:</xsl:text>
                <xsl:value-of select="date/@to"/>
            </xsl:if>
            <xsl:if test="@ref!= ''">
                <xsl:text>|ref:</xsl:text>
                <xsl:value-of select="@ref"/>
            </xsl:if>
            
            <xsl:if test="following-sibling::*[not(self::note)]/normalize-space() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="roleName">
        <xsl:if test="text() != ''" >
            <xsl:text>roleName: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            
            <xsl:if test="date/@from != ''">
                <xsl:text>|from:</xsl:text>
                <xsl:value-of select="date/@from"/>
            </xsl:if>         
            <xsl:if test="date/@to != ''">
                <xsl:text>|to:</xsl:text>
                <xsl:value-of select="date/@to"/>
            </xsl:if>
            <xsl:text> ## </xsl:text>
<!--            <xsl:if test="following-sibling::roleName/normalize-space() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>-->
        </xsl:if>
    </xsl:template>
 
    <xsl:template match="affiliation">
        <xsl:if test="text() != ''" >
            <xsl:text>affiliation: </xsl:text>
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>

            
            <xsl:if test="@type != ''">
                <xsl:text>|type:</xsl:text>
                <xsl:value-of select="@type"/>
            </xsl:if>         
        </xsl:if>
    </xsl:template>
        
        
    <xsl:template match="note">
        <xsl:if test="text() != ''" >
            <xsl:value-of select="normalize-space(translate(.,'&#xA;',''))"/>
            <xsl:if test="persName">
                <xsl:text> |persRef:</xsl:text>
                <xsl:for-each select="persName">
                    <xsl:value-of select="@ref"/>
                </xsl:for-each>
<!--                    <xsl:text>|persRef:</xsl:text>
                    <xsl:value-of select="@ref"/>
<!-\-                    <xsl:choose>
                        <!-\\- if not last -\\->
                        <xsl:when test="position() != last()">
                            <xsl:text>|persRef:</xsl:text>
                            <xsl:value-of select="@ref"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>|persRef:</xsl:text>
                            <xsl:value-of select="@ref"/>
                            <xsl:text>|</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>-\->
                </xsl:for-each>-->
            </xsl:if>
            <xsl:if test="following-sibling::note/normalize-space() != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="ptr">
        <xsl:if test="@target != ''" >
            <xsl:value-of select="@target"/>
            <xsl:if test="following-sibling::ptr/@target != ''">
                <xsl:text> ## </xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="note/persName">
        <xsl:if test="@ana != ''" >
            <xsl:value-of select="@ana"/>
            
            <xsl:choose>
                <xsl:when test="following-sibling::persName/@ana != ''">
                    <xsl:text> ## </xsl:text>
                </xsl:when>
                <xsl:when test="parent::note/following-sibling::note/persName/@ana != ''">
                    <xsl:text> ## </xsl:text>
                </xsl:when>
                <xsl:otherwise></xsl:otherwise> <!-- ne rien faire -->
                
            </xsl:choose>
        </xsl:if>
    </xsl:template>
    
</xsl:stylesheet>