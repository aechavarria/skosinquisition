<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
    <!--xmlns:tei="http://www.tei-c.org/ns/1.0"-->
    <!--    -->
    
    <xsl:output method="xml" encoding="UTF-8"/>

    <xsl:key name="personid" match="person" use="@xml:id"/>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="persName[@ref]">
        <persName xmlns="http://www.tei-c.org/ns/1.0" ref="{@ref}" ana="{key('personid', substring-after(@ref, '#'))/@ana}" >     
            <xsl:apply-templates select="normalize-space(translate(.,'&#xA;',''))"/>
        </persName>
    </xsl:template>
    
</xsl:stylesheet>