<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"

    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" encoding="UTF-8"/>
    
    <xsl:key name="personid" match="person" use="@xml:id"/>
    
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="listPerson">
        <listPerson ana="{count(preceding::listPerson) + 1 + 1000}" xmlns="http://www.tei-c.org/ns/1.0">
            <xsl:apply-templates select="node()|@*"/>
             </listPerson>
    </xsl:template>
    
    <xsl:template match="person">
            <person xml:id="{@xml:id}" ana="{count(preceding::person) + 1 + 10000}" xmlns="http://www.tei-c.org/ns/1.0">
            <!--<xsl:copy-of select="./*"/> -->
                    <!-- Si utilise ./* ou même * : ne copie pas les @ du même niveau, slmt ceux des enfants : faut utiliser 
                            <xsl:apply-templates select="node()|@*"/>
                    -->
            <xsl:apply-templates select="node()|@*"/>
             </person>
    </xsl:template>
</xsl:stylesheet>