import csv
from rdflib import Graph, URIRef, Literal
from rdflib.namespace import SKOS, RDF
import unidecode
import re

def to_camel_case_spanish(label):
    label = unidecode.unidecode(label)
    words = label.split()
    filtered_words = [word for word in words if len(word) > 3]
    return ''.join(word.capitalize() if i > 0 else word for i, word in enumerate(filtered_words))

def clean_scope_note(text):
    # Eliminar todo lo que está después de 'Bibliografía'
    text = re.split(r'Bibliograf[ií]a', text, flags=re.IGNORECASE)[0]
    
    # Eliminar etiquetas HTML
    text = re.sub(r'<[^>]+>', '', text)
    
    # Eliminar espacios extra y líneas en blanco
    text = re.sub(r'\s+', ' ', text).strip()
    
    return text

# Cargar el archivo RDF
g = Graph()
g.parse("RDFtoTEI/RDFtoHeuristVoc/InqEspDoc.rdf", format="application/rdf+xml")

# Preparar el archivo CSV
csv_file = 'RDFtoTEI/RDFtoHeuristVoc/vocabulario_heurist.csv'
csv_headers = ['Term', 'Description', 'StandardCode', 'ReferenceURI']

with open(csv_file, 'w', newline='', encoding='utf-8') as file:
    writer = csv.DictWriter(file, fieldnames=csv_headers)
    writer.writeheader()

    # Iterar sobre los conceptos SKOS
    for subj in g.subjects(RDF.type, SKOS.Concept):
        # Obtener el prefLabel en español
        pref_label_es = next((o for o in g.objects(subj, SKOS.prefLabel) 
                              if isinstance(o, Literal) and o.language == 'es'), None)
        
        if pref_label_es:
            # Obtener la scopeNote en español
            scope_note_es = next((o for o in g.objects(subj, SKOS.scopeNote) 
                                  if isinstance(o, Literal) and o.language == 'es'), '')
            
            # Limpiar la scopeNote
            clean_description = clean_scope_note(str(scope_note_es))
            
            # Generar el StandardCode
            standard_code = to_camel_case_spanish(str(pref_label_es))
            
            # Obtener el ReferenceURI
            reference_uri = str(subj)
            
            # Escribir la fila en el CSV
            writer.writerow({
                'Term': str(pref_label_es),
                'Description': clean_description,
                'StandardCode': standard_code,
                'ReferenceURI': reference_uri
            })

print(f"El archivo CSV ha sido creado: {csv_file}")